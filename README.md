# core-api
Is entry point for the system, communicates with scriptor-api service for retrieving the scripts, splitting in kafka messages every word from received script and puts on kafka topic, that is consumed by translator-api, which  translates each word and saves to the mongoDb data 

## Run local infrastructure( zookeeper, kafka, it's topic "test" and replication factor set to 1 ): ##
> cd local-infrastructure && docker-compose up

## Run cmd producer: ##
> cd local-infrastructure && kafka-producer.sh

## Run cmd a consumer: ##
> cd local-infrastructure && kafka-consumer.sh

## Run all the services(core-api, translator-api, scripts-api, zookeeper, kafka, mongoDb) ##
> cd local-infrastructure && run-all.sh

## Connect to mongoDb with credentials: ##
> username: admin <br/>
> password: admin <br/>
> host: localhost <br/>
> database: test <br/>
> port: 27017 <br/>
> authentication-database: admin <br/>

## Retrieve data from mongoDb: ##
> test.getCollection('wordDoc').find({}).limit(501)
