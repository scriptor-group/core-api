package com.scriptor.coreapi.controller;

import com.scriptor.coreapi.service.MovieService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class MovieController {
    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/script/{movieTitle}/{lang}")
    public void translateMovie(@PathVariable String movieTitle, @PathVariable String lang, @RequestHeader(value = "userId") String userId) {
        log.info("getScript " + movieTitle + userId + lang);
        movieService.translateMovie(movieTitle, userId, lang);
    }
}
