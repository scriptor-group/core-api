package com.scriptor.coreapi.service;

import io.micrometer.core.instrument.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.function.Supplier;

@Service
@Slf4j
public class ScriptService {
    private final RestTemplate restTemplate;
    @Value("${scriptservice.url}")
    private String url;

    public ScriptService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public String getScripts(String movieTitle) {
        return Optional.ofNullable(
                wrapRestClientExceptionWithNullReturn(() ->
                        restTemplate.getForEntity(url + "/script/" + movieTitle, String.class).getBody()))
                .filter(StringUtils::isNotBlank)
                .orElse(null);
    }

    private String wrapRestClientExceptionWithNullReturn(Supplier<String> stringSupplier) {
        try {
            return stringSupplier.get();
        } catch (RestClientException e) {
            log.info("RestClientException " + e);
            return null;
        }
    }
}
