package com.scriptor.coreapi.service;

import com.scriptor.coreapi.dto.WordDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.Optional;
import java.util.stream.Stream;

@Service
@Slf4j
public class MovieService {
    private final ScriptService scriptService;
    private final KafkaTemplate<String, WordDto> kafkaTemplate;
    @Value("${kafka.topic}")
    private String topic;

    public MovieService(ScriptService scriptService, KafkaTemplate<String, WordDto> kafkaTemplate) {
        this.scriptService = scriptService;
        this.kafkaTemplate = kafkaTemplate;
    }

    public void translateMovie(String title, String userId, String lang) {
        Optional.ofNullable(scriptService.getScripts(title)).ifPresent(scripts ->
                Stream.of(scripts.split("\\W+")).forEach(
                        s -> {
                            log.info("sending : " + s);
                            ListenableFuture<SendResult<String, WordDto>> future = kafkaTemplate.send(
                                    topic,
                                    WordDto.builder()
                                            .userId(userId)
                                            .lang(lang)
                                            .word(s)
                                            .build());
                            future.addCallback(kafkaloggingCallback());
                        }));
    }

    private ListenableFutureCallback<SendResult<String, WordDto>> kafkaloggingCallback() {
        return new ListenableFutureCallback<SendResult<String, WordDto>>() {
            @Override
            public void onSuccess(SendResult<String, WordDto> result) {
                log.info("Accepted: " + result.toString());
            }

            @Override
            public void onFailure(Throwable ex) {
                log.info("Not Accepted: " + ex);
            }
        };
    }
}
