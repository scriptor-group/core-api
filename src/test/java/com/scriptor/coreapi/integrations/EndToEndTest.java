package com.scriptor.coreapi.integrations;

import com.scriptor.coreapi.dto.WordDto;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureMockRestServiceServer;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@AutoConfigureMockRestServiceServer
@AutoConfigureWebClient(registerRestTemplate = true)
@EmbeddedKafka(partitions = 1, topics = EndToEndTest.TOPIC,
        bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@DirtiesContext
class EndToEndTest {
    private static final String MOVIE_TITLE = "Batman";
    public static final String SCRIPT_CONTENT = "Script content";
    public static final String SCRIPT_CONTENT_WORD_1 = "Script";
    public static final String SCRIPT_CONTENT_WORD_2 = "content";
    public static final String TOPIC = "test";
    public static final String USER_ID = "userId";
    public static final String LANG = "lang";

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private EmbeddedKafkaBroker embeddedKafka;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @Value("${scriptservice.url}")
    private String url;

    @Test
    void getScripts_success() throws Exception {
        mockRestServiceServer.expect(ExpectedCount.once(),
                requestTo(url + "/script/" + MOVIE_TITLE)).andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(SCRIPT_CONTENT, MediaType.APPLICATION_JSON));

        Map<String, Object> configs = KafkaTestUtils.consumerProps("test1", "false", embeddedKafka);
        configs.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        configs.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        JsonDeserializer<WordDto> wordDtoJsonDeserializer = new JsonDeserializer<>(WordDto.class, false);

        DefaultKafkaConsumerFactory<String, WordDto> defaultKafkaConsumerFactory = new DefaultKafkaConsumerFactory<>(
                configs,
                new StringDeserializer(),
                wordDtoJsonDeserializer
        );
        ContainerProperties containerProperties = new ContainerProperties(TOPIC);
        KafkaMessageListenerContainer<String, WordDto> container =
                new KafkaMessageListenerContainer<>(defaultKafkaConsumerFactory, containerProperties);
        List<WordDto> actualRecordList = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(2);
        container.setupMessageListener((MessageListener<String, WordDto>) record -> {
            actualRecordList.add(record.value());
            latch.countDown();
        });
        container.start();
        ContainerTestUtils.waitForAssignment(container, embeddedKafka.getPartitionsPerTopic());

        mockMvc.perform(get("/script/{movieTitle}/{lang}", MOVIE_TITLE, LANG)
                .header("userId", USER_ID))
                .andExpect(status().isOk());

        WordDto expectedWord1 = WordDto.builder().userId(USER_ID).lang(LANG).word(SCRIPT_CONTENT_WORD_1).build();
        WordDto expectedWord2 = WordDto.builder().userId(USER_ID).lang(LANG).word(SCRIPT_CONTENT_WORD_2).build();
        assertTrue(latch.await(5, TimeUnit.SECONDS));
        assertThat(actualRecordList, containsInAnyOrder(expectedWord1, expectedWord2));
    }

}
