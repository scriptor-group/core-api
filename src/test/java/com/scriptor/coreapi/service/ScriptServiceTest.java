package com.scriptor.coreapi.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ScriptServiceTest {
    public static final String MOVIE_TITLE = "movieTitle";
    public static final String EXPECTED_SCRIPT = "expected script value";
    public static final String URL = "url";
    @InjectMocks
    private ScriptService scriptService;

    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(scriptService, "url", URL);
    }

    @Test
    void getScripts_successful() {
        when(restTemplate.getForEntity( "url/script/movieTitle", String.class)).thenReturn(new ResponseEntity<>(EXPECTED_SCRIPT, HttpStatus.OK));
        String actualScripts = scriptService.getScripts(MOVIE_TITLE);
        assertEquals(actualScripts, EXPECTED_SCRIPT);
    }

    @Test
    void getScripts_nullReturn() {
        when(restTemplate.getForEntity( "url/script/movieTitle", String.class)).thenReturn(new ResponseEntity<>(null, HttpStatus.OK));
        String actualScripts = scriptService.getScripts(MOVIE_TITLE);
        assertNull(actualScripts);
    }

    @Test
    void getScripts_emptyString() {
        when(restTemplate.getForEntity( "url/script/movieTitle", String.class)).thenReturn(new ResponseEntity<>("", HttpStatus.OK));
        String actualScripts = scriptService.getScripts(MOVIE_TITLE);
        assertNull(actualScripts);
    }

    @Test
    void getScripts_restException() {
        when(restTemplate.getForEntity( "url/script/movieTitle", String.class)).thenThrow(new RestClientException("Server is down"));
        String actualScripts = scriptService.getScripts(MOVIE_TITLE);
        assertNull(actualScripts);
    }
}

