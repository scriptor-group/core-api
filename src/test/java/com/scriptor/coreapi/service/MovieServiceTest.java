package com.scriptor.coreapi.service;

import com.scriptor.coreapi.dto.WordDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MovieServiceTest {
    private static final String MOVIE_TITLE = "Batman";
    public static final String SCRIPT_CONTENT = "Script content";
    public static final String SCRIPT_CONTENT_WORD_1 = "Script";
    public static final String SCRIPT_CONTENT_WORD_2 = "content";
    public static final String TOPIC = "topic";
    public static final String USER_ID = "userId";
    public static final String LANG = "lang";

    @InjectMocks
    private MovieService movieService;

    @Mock
    private ScriptService scriptService;
    @Mock
    private KafkaTemplate<String, WordDto> kafkaTemplate;

    @Mock
    private SendResult<String, WordDto> sendResult;
    @Mock
    private ListenableFuture<SendResult<String, WordDto>> responseFuture;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(movieService, "topic", TOPIC);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void translateMovie_success() {
        when(scriptService.getScripts(MOVIE_TITLE)).thenReturn(SCRIPT_CONTENT);

        WordDto word1 = WordDto.builder().userId(USER_ID).lang(LANG).word(SCRIPT_CONTENT_WORD_1).build();
        WordDto word2 = WordDto.builder().userId(USER_ID).lang(LANG).word(SCRIPT_CONTENT_WORD_2).build();

        when(kafkaTemplate.send(TOPIC, word1)).thenReturn(responseFuture);
        when(kafkaTemplate.send(TOPIC, word2)).thenReturn(responseFuture);

        doAnswer(futureInvocation -> {
            ListenableFutureCallback<SendResult<String, WordDto>> listenableFutureCallback = futureInvocation.getArgument(0);
            listenableFutureCallback.onSuccess(sendResult);
            return null;
        }).when(responseFuture).addCallback(any(ListenableFutureCallback.class));


        movieService.translateMovie(MOVIE_TITLE, USER_ID, LANG);

        verify(kafkaTemplate).send(TOPIC, word1);
        verify(kafkaTemplate).send(TOPIC, word2);

    }

    @Test
    public void translateMovie_nullMovieScriptsFound() {
        when(scriptService.getScripts(MOVIE_TITLE)).thenReturn(null);

        movieService.translateMovie(MOVIE_TITLE, USER_ID, LANG);

        verify(kafkaTemplate, never()).send(eq(TOPIC), any());
    }

    @Test
    public void translateMovie_kafkaFailure() {
        when(scriptService.getScripts(MOVIE_TITLE)).thenReturn(SCRIPT_CONTENT);

        WordDto word1 = WordDto.builder().userId(USER_ID).lang(LANG).word(SCRIPT_CONTENT_WORD_1).build();
        WordDto word2 = WordDto.builder().userId(USER_ID).lang(LANG).word(SCRIPT_CONTENT_WORD_2).build();

        when(kafkaTemplate.send(TOPIC, word1)).thenReturn(responseFuture);
        when(kafkaTemplate.send(TOPIC, word2)).thenReturn(responseFuture);

        doAnswer(futureInvocation -> {
            ListenableFutureCallback<SendResult<String, WordDto>> listenableFutureCallback = futureInvocation.getArgument(0);
            listenableFutureCallback.onFailure(new RuntimeException());
            return null;
        }).when(responseFuture).addCallback(any(ListenableFutureCallback.class));

        movieService.translateMovie(MOVIE_TITLE, USER_ID, LANG);

        verify(kafkaTemplate).send(TOPIC, word1);
        verify(kafkaTemplate).send(TOPIC, word2);
    }
}
