package com.scriptor.coreapi.controller;

import com.scriptor.coreapi.service.MovieService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class MovieControllerTest {
    private static final String MOVIE_TITLE = "Batman";
    public static final String USER_ID = "userId";
    public static final String LANG = "it";

    @InjectMocks
    private MovieController movieController;

    @Mock
    private MovieService movieService;

    @Test
    void translateMovie_success() {
        movieController.translateMovie(MOVIE_TITLE, LANG, USER_ID);
        verify(movieService).translateMovie(MOVIE_TITLE, USER_ID, LANG);
    }
}
